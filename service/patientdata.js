const { response } = require("../app")
var connection = require("../connections/dbconnection")

//get all users data 
var getdata = function (req, res) {
    let query = "select *from usersdetails"
    connection.query(query, function (err, data) {
        if (!err) {
            const resp =
            {
                'status': 'All the users data get successfully',
                'respones': data
            }

            res.send(resp)
            // res.send(data)
        }
        else {
            const resp = {
                'status': 'failed',
                'respones': err
            }
            res.status(400).send(resp)
        }
    })
}


// post the users data   
const currentDate = new Date();
const date = (` ${currentDate.getDate()}`)
const month = (`${currentDate.getMonth() + 1}`)
const year = currentDate.getFullYear();

var CreatedDate = `${date}-${month}-${year}`

var postpatiendata = function (req, res) {

    let query = "insert into usersdetails (Firstname, Middlename, LastName, Email, PhoneNumber, Age, Role, CreatedBy, username, password, CreatedDate, Gender, Addressline1, Addressline2, State, Pincode, AlternativeNumber,City,Country,Bloodgroup,patientproblem,height,weight,designation,availbility,appointment,doc_prescription_1 ,doc_prescription_2) values ('" + req.body.Firstname + "','" + req.body.Middlename + "','" + req.body.LastName + "','" + req.body.Email + "','" + req.body.PhoneNumber + "','" + req.body.Age + "','" + req.body.Role + "','" + req.body.CreatedBy + "','" + req.body.username + "','" + req.body.password + "','" + CreatedDate + "','" + req.body.Gender + "','" + req.body.Addressline1 + "','" + req.body.Addressline2 + "','" + req.body.State + "','" + req.body.Pincode + "','" + req.body.AlternativeNumber + "','" + req.body.City + "','" + req.body.Country + "','" + req.body.Bloodgroup + "','" + req.body.patientproblem + "','" + req.body.height + "','" + req.body.weight + "','" + req.body.designation + "','" + req.body.availbility + "','" + req.body.appointment + "','" + req.body.doc_prescription_1 + "','" + req.body.doc_prescription_2 + "')"

    connection.query(query, function (err, data) {
        if (!err) {
            const resp = {
                'status': 'sucess',
                'respones': 'Successfully inserted the Record',
                'recordId': data.insertId
            }

            res.send(resp)
        }
        else {
            console.log(err)
        }
    })
}


var updatedata = (req, res) => {
    var Id = req.body.Id
    var Firstname = req.body.Firstname
    var Middlename = req.body.Middlename
    var LastName = req.body.LastName
    var Email = req.body.Email
    var PhoneNumber = req.body.PhoneNumber
    var Age = req.body.Age
    var Role = req.body.Role
    var CreatedBy = req.body.CreatedBy
    var username = req.body.username
    var password = req.body.password
    var Gender = req.body.Gender
    var Addressline1 = req.body.Addressline1
    var Addressline2 = req.body.Addressline2
    var State = req.body.State
    var Pincode = req.body.Pincode
    var AlternativeNumber = req.body.AlternativeNumber
    var City = req.body.City
    var Country = req.body.Country
    var Bloodgroup = req.body.Bloodgroup
    var patientproblem = req.body.patientproblem
    var height = req.body.height
    var weight = req.body.weight
    var designation = req.body.designation
    var availbility = req.body.availbility
    var appointment = req.body.appointment
    var doc_prescription_1 = req.body.doc_prescription_1
    var doc_prescription_2 = req.body.doc_prescription_2

    let sql = "UPDATE usersdetails set Firstname=?, Middlename=?, LastName=?, Email=?, PhoneNumber=?, Age=?, Role=?, CreatedBy=?, username=?, password=?, Gender=?, Addressline1=?, Addressline2=?, State=?, Pincode=?, AlternativeNumber=?, City=?, Country=?, Bloodgroup=?,patientproblem=?, height=?, weight=?, designation=?,availbility=?,appointment=?,doc_prescription_1=?,doc_prescription_2=? where Id=?";
    console.log('update query', sql)
    connection.query(sql, [Firstname, Middlename, LastName, Email, PhoneNumber, Age, Role, CreatedBy, username, password, Gender, Addressline1, Addressline2, State, Pincode, AlternativeNumber, City, Country, Bloodgroup, patientproblem, height, weight, designation, availbility, appointment, doc_prescription_1, doc_prescription_2, Id], function (err, data) {

        if (!err) {
            res.send(data)


        }
        else {
            res.send(err)

        }
    }
    )
}



var deletepatient = function (req, res) {
    console.log("delectttt", req.params.Id)
    var delquery = "DELETE FROM usersdetails WHERE Id=" + req.params.Id;
    connection.query(delquery, (err) => {

        if (!err) {
            res.send("Deleted sucessfully")
        }
        else {
            console.log(err);
        }
    });
}


//=======>Doctor screen with patient details API<========

//get the patient data 
var getlistofdata = function (req, res) {
    let query = "select *from listofpatients"
    connection.query(query, function (err, data) {
        if (!err) {
            //    res.send(data)
            const resp = {
                'status': 'All the patients data get successfully',
                'responese': data
            }
            res.send(resp.responese)
        }
        else {
            res.status(400).send(err)
        }
    })
}

//posting patient data 
var postpatient = function (req, res) {
    let query = "insert into listofpatients (patientname, phonenumber, address, appointment, patientproblem) values ('" + req.body.patientname + "','" + req.body.phonenumber + "','" + req.body.address + "','" + req.body.appointment + "','" + req.body.patientproblem + "')";
    console.log("query values", query)
    connection.query(query, function (err, data) {
        if (!err) {
            const resp = {
                'status': 'patient added successfully',
                'responese': data
            }
            res.send(resp)
        }
        else {
            console.log(err)
        }
    })
}

//update the patient data 
//patientname, phonenumber, address, appointment, patientproblem
var updatepatientdata = (req, res) => {
    var sno = req.body.sno
    var patientname = req.body.patientname
    var phonenumber = req.body.phonenumber
    var address = req.body.address
    var appointment = req.body.appointment
    var patientproblem = req.body.patientproblem


    let sql = "UPDATE listofpatients set patientname=?, phonenumber=?, address=?, appointment=?, patientproblem=?  where sno=?";

    connection.query(sql, [patientname, phonenumber, address, appointment, patientproblem, sno], function (err, data) {

        if (!err) {
            res.send(data)
        }
        else {
            res.send(err)
        }
    }
    )
}

//Delete patient from doctor screen
var delpatient = function (req, res) {
    var delquery = "DELETE FROM listofpatients WHERE sno=" + req.params.sno;
    connection.query(delquery, (err) => {

        if (!err) {
            res.send("Deleted sucessfully")
            // const resp = {
            //         'status':'patient data deleted',
            //         'responese':data
            //         }
            //     res.send(resp.responese)    
        }
        else {
            console.log(err);
        }
    });
}

//getting data based on the role

var getdoctor = function (req, res) {

    var docquery = "SELECT * FROM usersdetails WHERE Role ='" + req.params.Role + "'";
    connection.query(docquery, (err, result) => {
        if (err) {
            throw err;
            return res.status(400).send({
                msg: err
            });
        }
        else {
            const data = {
                status: 'success',
                response: result
            }
            res.send(data)
        }

    })


}


//username and password request

var responsetotaldata = function (req, res) {

    var resquery = "SELECT * FROM usersdetails WHERE username = '" + req.body.username + "'";
    connection.query(resquery, (err, result) => {
        if (err) {
            throw err;
            return res.status(400).send({
                msg: err
            });
        }
        if (!result.length) {
            return res.status(401).send({

                msg: 'please enter valid username'
            });
        }
        // res.send(result)
        const data = {
            status: 'success',
            response: result
        }
        res.send(data)
        return res.status(401).send({
            msg: 'Username or password is incorrect!'
        });
    }
    );
}

//appointments

  var storingofids=function(req, res) { 
    console.log(req.body)
    let query = "insert into appointments (patientId,doctorId) values ('" + req.body.patiendId+ "','" + req.body.doctorId + "')";
    console.log("query values", query)
    connection.query(query, function (err, data) {
        if (!err)
        {
            const resp = {
                'status':'Doctor slot bokked successfuly',
                'responese':data
                }
            res.send(resp)
        }   
        else 
        {
            console.log(err)
        }
    })
}

var getPatientsList =function(req, res) {
    console.log(req.params.doctorId)
    let query="select *from appointments"
    connection.query(query,function(err,data){
          if(!err)
        {
        console.log(data ,req.params.doctorId)
 var plist = data.filter(e=> e.doctorId == req.params.doctorId).map(e=>e.patientId)
  console.log(plist)

  let query="select *from usersdetails"
  connection.query(query,function(err,data){
        if(!err)
      {
        console.log(plist)
         console.log(data)
        //  plist.filter(e=>)
    //   res.send(resp)
      // res.send(data)
      }
  })
        // res.send(data)
        }
        else 
        {
            const resp = {
            'status':'failed',
            'respones':err
            }
           res.status(400).send(resp)
        }
    })
}


//

var joinssql=function(req, res) { 
    console.log(req.body)
    let query = "SELECT * FROM `usersdetails` CROSS JOIN `appointments`";
    console.log("query values", query)
    connection.query(query, function (err, data) {
        if (!err)
        {
            const resp = {
                'status':'success',
                'responese':data
                }
            res.send(resp)
        }   
        else 
        {
            console.log(err)
        }
    })
}






module.exports = {joinssql:joinssql,getPatientsList:getPatientsList,storingofids:storingofids, responsetotaldata: responsetotaldata, getdoctor: getdoctor, updatepatientdata: updatepatientdata, delpatient: delpatient, postpatient: postpatient, getdata: getdata, deletepatient: deletepatient, postpatiendata: postpatiendata, updatedata: updatedata, getlistofdata: getlistofdata }





