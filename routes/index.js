var express = require('express');
var router = express.Router();
var service=require('../service/patientdata');
var cors = require('cors');
const app = require('../app');
router.use(cors())

var corsOptions =
{
    origin: '*'
}
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


// Registration API's
router.get('/listofusers',cors(corsOptions),service.getdata)
router.post('/postuserdata',cors(corsOptions),service.postpatiendata)
router.put('/updateuserdata',cors(corsOptions),service.updatedata)
router.delete('/deleteuser/:Id',cors(corsOptions),service.deletepatient)



//Doctors screen- Patient Data display
router.get('/getlistofpatients',cors(corsOptions),service.getlistofdata)
router.post('/postpatientdata',cors(corsOptions),service.postpatient)
router.delete('/deletepatient/:sno',cors(corsOptions),service.delpatient)
router.put('/updatepatientdata',cors(corsOptions),service.updatepatientdata)

//get the data of patients or doctors
router.get('/listofrole/:Role',cors(corsOptions),service.getdoctor)

//sending total data as a based on username and password
router.post('/login',cors(corsOptions),service.responsetotaldata)


//storing the doctorId and patientId in table
router.post('/appointments',cors(corsOptions),service.storingofids)
router.get('/getPatientsList/:doctorId',cors(corsOptions),service.getPatientsList)
module.exports = router;





